package solutions;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
 
 
public class week2 extends JPanel {
 
	@Override
	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D)g;
		Grid test2 = new Grid(20, 20, 720, 720);
		g2.translate(10, 10);
		drawGrid(test2, g2);
		g2.translate(-10, -10);
	}
	
	public void drawGrid(Grid grid, Graphics2D g2) {
		for(int i=0; i<grid.getRows(); i++) {
			for(int k=0; k<grid.getColumns(); k++) {
				drawCell(grid.getCell(i, k), g2);
			}
		}
	}
	
	public void drawCell(Cell c, Graphics2D g2) {
		g2.drawRect(c.getX(), c.getY(), c.getWidth(), c.getHeight());
	}
	
	
	public static void main(String [] args) {
		JFrame frame = new JFrame("Main");
		week2 p = new week2();
		p.setPreferredSize(new Dimension(1280, 720));
		frame.add(p);
		frame.pack();
		frame.setVisible(true);
	}
 
}