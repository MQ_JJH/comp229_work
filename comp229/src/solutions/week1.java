package solutions;

public class week1 {
	/*
	 
Take a look a the two repositories:
  
  * (A) https://bitbucket.org/farleyknight/ruby-git
  * (B) https://bitbucket.org/kennethendfinger/git-repo

And answer the following questions about them:

  * Who made the last commit to repository A?
	farleyknight
  * Who made the first commit to repository A?
	Scott Chacon
  * Who made the last and first commits to repository B?
	Last Commit: 	Shawn O. Pearce
	First Commit:	The Android Open Source Project executed by Shawn O. Pearce
  * Who has been the most active recent contributor on repository A?  How about repository B?
	Repo A: Farley Knight
	Repo B:	Shawn O. Pearce
  * Are either/both of these projects active at the moment?  🤔 If not, what do you think happened?
	No, the programs fulfilled their intended purpose and the community is dead
	Either that or the developer stopped for personal reasons and abandoned the project
	
  * 🤔 Which file in each project has had the most activity?
	Repo A: lib.rb
	Repo B: project.py
	Used command:
git log --pretty=format: --name-only | sort | uniq -c |sort -rg | head -10
	 */
	public static void main(String[] args) {
		System.out.println("~~~~~\nSheep and Wolves\n~~~~~");
	}

}
