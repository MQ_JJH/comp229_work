package solutions;

public class Grid {
	private int rows, cols;
	private Cell[][] arr;
	
	public Grid(int a, int b, int width, int height) {
		rows = a;
		cols = b;
		arr = new Cell[Math.abs(a)][Math.abs(b)];
		for(int i=0; i<a; i++) {
			for(int k=0; k<b; k++) {
				int posX = i*(width/a);
				int posY = k*(height/b);
				arr[i][k] = new Cell(posX, posY, width/a, height/b);
			}
		}
	}
	
	public Cell getCell(int x, int y) {
		return arr[x][y];
	}
	
	public int getRows() {
		return rows;
	}
	
	public int getColumns() {
		return cols;
	}
}
